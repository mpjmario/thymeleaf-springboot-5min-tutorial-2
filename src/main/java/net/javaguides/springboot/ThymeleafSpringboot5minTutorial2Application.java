package net.javaguides.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeleafSpringboot5minTutorial2Application {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafSpringboot5minTutorial2Application.class, args);
	}

}
